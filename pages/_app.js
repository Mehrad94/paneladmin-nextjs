import App from "next/app";
import Head from "next/head";
import "../public/styles/index.scss";
import PanelScreen from "../panelAdmin/screen/PanelScreen";
import WebsiteScreen from "../website/screen/WebsiteScreen";
import withRedux from "next-redux-wrapper";
import configureStore from "../store";
import { Provider } from "react-redux";
import ErrorHandler from "../website/baseComponents/ErrorHandler";
class RimtalApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps({ ctx });
    }

    return { pageProps };
  }
  render() {
    const { Component, pageProps, store, router } = this.props;
    let body;
    if (router.asPath.includes("/panelAdmin")) {
      body = (
        <PanelScreen router={router}>
          <Component {...pageProps} />
        </PanelScreen>
      );
    } else {
      body = (
        <WebsiteScreen>
          <Component {...pageProps} />
        </WebsiteScreen>
      );
    }
    return (
      <div>
        <Head>
          <title>پروژه خام (ریداکس)</title>
          <meta name="description" content="جامع ترین پلتفرم موسیقی ایران ." />
          <meta name="keywords" content="ریمتال،rimtal,RIMTAL ,موسیقی و ,IPTV,موسیقی,پرداخت اجتماعی،پخش زنده،اپلیکیشن,سرگرمی," />
          <meta content="width=device-width, initial-scale=1" name="viewport" />
          <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossOrigin="anonymous"></script>
          <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossOrigin="anonymous"></script>
          <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossOrigin="anonymous" />

          {/* <link rel="stylesheet" href="react-toastify/dist/ReactToastify.css" /> */}
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossOrigin="anonymous"></script>
          {/* <link href={"/fonts/fontawesome/css/all.css"} rel={"stylesheet"} /> */}
          {/* <link href={"/fonts/index.css"} rel={"stylesheet"} /> */}
          <link href={"/styles/css/styles.css"} rel={"stylesheet"} />
        </Head>
        <div className="base-page">
          <Provider store={store}>
            {body} <ErrorHandler />
          </Provider>
        </div>
      </div>
    );
  }
}

export default withRedux(configureStore)(RimtalApp);
