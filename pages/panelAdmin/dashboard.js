import React, { useEffect } from "react";
import DashboardScreen from "../../panelAdmin/screen/DashboardScreen";
import { useSelector, useDispatch } from "react-redux";
// import * as sagaActions from "../../store/actions/saga";
// import * as reduxActions from "../../store/actions/redux";
import panelAdmin from "../../panelAdmin";
const DashBoard = () => {
  const sagaActions = panelAdmin.actions.sagaActions;
  const reduxActions = panelAdmin.actions.reduxActions;
  const dispatch = useDispatch();
  const store = useSelector((state) => {
    return state;
  });
  useEffect(() => {
    dispatch(reduxActions.setPageName(panelAdmin.values.strings.DASHBOARD));
  }, []);
  return <DashboardScreen />;
};
export default DashBoard;
