import get from "./Get";
import put from "./Put";
import post from "./Post";
import patch from "./Patch";
import deletes from "./Delete";

export { get, post, put, patch, deletes };
