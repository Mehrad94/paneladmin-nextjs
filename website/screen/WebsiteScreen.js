import React from "react";

const WebsiteScreen = (props) => {
  return <div>{props.children}</div>;
};

export default WebsiteScreen;
