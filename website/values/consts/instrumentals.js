const instrumentals = [
  { titleFa: "پیانو", titleEn: "Piano", icon: "far fa-piano" },
  { titleFa: "چمپن استیک", titleEn: "Chapman Stick", icon: "far fa-guitar-electric" },
  { titleFa: "گیتار", titleEn: "Guitar", icon: "fal fa-guitar" },
  { titleFa: "ساکسیفون", titleEn: "Saxophone", icon: "fal fa-saxophone" },
  { titleFa: "پیانو", titleEn: "Piano", icon: "far fa-piano" },
  { titleFa: "چمپن استیک", titleEn: "Chapman Stick", icon: "far fa-guitar-electric" },
  { titleFa: "گیتار", titleEn: "Guitar", icon: "fal fa-guitar" },
  { titleFa: "ساکسیفون", titleEn: "Saxophone", icon: "fal fa-saxophone" },
  { titleFa: "پیانو", titleEn: "Piano", icon: "far fa-piano" },
  { titleFa: "چمپن استیک", titleEn: "Chapman Stick", icon: "far fa-guitar-electric" },
  { titleFa: "گیتار", titleEn: "Guitar", icon: "fal fa-guitar" },
  { titleFa: "ساکسیفون", titleEn: "Saxophone", icon: "fal fa-saxophone" },
];
export default instrumentals;
