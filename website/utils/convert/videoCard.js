import website from "./../..";

const videoCard = (data) => {
  let convertData = [];
  // console.log({ mood: data });
  for (const index in data) {
    let noEntries = website.values.strings.NO_ENTRIED;
    let title = data[index].title ? data[index].title : noEntries;
    let artist = data[index].artist ? data[index].artist : noEntries;
    let releaseDate = data[index].releaseDate ? data[index].releaseDate : "0";
    let genres = data[index].genres ? data[index].genres : "0";
    convertData.push({
      titleTop: title,
      titleMiddle: artist,
      titleBottom: [releaseDate, genres[0]],
      images: data[index].images,
      location: { href: "/video", as: `/video#` },
    });
  }
  return convertData;
};
export default videoCard;
