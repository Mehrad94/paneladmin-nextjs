const convertToDate = (selectedDay) => {
  let day = selectedDay.day;
  let month = selectedDay.month;
  let year = selectedDay.year;
  let date = year + "/" + month + "/" + day;
  return date;
};
export default convertToDate;
