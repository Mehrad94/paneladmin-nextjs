import React from "react";

const ShowScenario = (scenario) => {
  const thead = ["#", "نام", "تاریخ شروع ", " تاریخ پایان", "تعداد برندگان ", "شركت كنندگان", "برندگان", "جوایز", "پوچ ها", "زمان چرخش مجدد (روز)", "وضعیت", "ویرایش"];
  let tbody = [];
  for (let index = 0; index < scenario.length; index++) {
    let active = <i style={{ fontSize: "1em", color: "green", fontWeight: "900" }} className="far fa-check-circle"></i>;
    let deActive = <i style={{ fontSize: "1em", color: "red", fontWeight: "900" }} className="fas fa-ban"></i>;

    tbody.push({
      data: [
        scenario[index].name,
        scenario[index].startDate,
        scenario[index].endDate,
        scenario[index].winnersCount,
        { option: { eye: true, name: "participants" } },
        { option: { eye: true, name: "winners" } },
        { option: { eye: true, name: "gifts" } },
        { option: { eye: true, name: "empty" } },
        scenario[index].spinRepeatTime,
        scenario[index].isActive ? active : deActive,
        { option: { edit: true } },
      ],
      style: { background: scenario[index].isActive ? "rgb(100, 221, 23,0.3)" : "" },
    });
  }
  return [thead, tbody];
};

export default ShowScenario;
