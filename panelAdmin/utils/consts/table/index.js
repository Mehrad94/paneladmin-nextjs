import genres from "./genres";
import showScenario from "./ShowScenario";
import ShowScenarioDataInModal from "./ShowScenarioDataInModal";
import ShowScenarioDataInModalTwo from "./ShowScenarioDataInModalTwo";
import showOwner from "./ShowOwner";
import ShowDiscount from "./ShowDiscount";
import ShowClub from "./ShowClub";
import showTransaction from "./ShowTransactions";
import transactionDiscount from "./transactionDiscount";
import members from "./members";
import memberTransaction from "./memberTransaction";
import memberDiscount from "./memberDiscount";
import country from "./country";
import instrument from "./instrument";
import song from "./song";
import artist from "./artist";

const table = {
  instrument,
  country,
  memberDiscount,
  memberTransaction,
  members,
  transactionDiscount,
  showTransaction,
  ShowClub,
  ShowDiscount,
  showOwner,
  showScenario,
  genres,
  ShowScenarioDataInModal,
  ShowScenarioDataInModalTwo,
  song,
  artist,
};
export default table;
