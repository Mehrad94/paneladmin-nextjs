const instrument = (data, acceptedCard) => {
  const cardFormat = [];
  console.log({ insss: data });

  for (let index in data) {
    let dataIndex = data[index];
    let web = dataIndex.web ? dataIndex.web : "";
    let phone = dataIndex.phone ? dataIndex.phone : "";
    let title = dataIndex.title ? dataIndex.title : "";
    // let images = { web, phone };
    // console.log(images);

    cardFormat.push({
      _id: data[index]._id,
      // isActive: data[index].isActive,
      isAccept: acceptedCard ? (acceptedCard.web === web || acceptedCard.phone === phone ? "activeImage" : "") : "",
      image: { value: { web, phone } },
      body: [
        {
          right: [
            { elementType: "text", value: title, style: { color: "black", fontSize: "0.67rem", fontWeight: "bold" } },
          ],
        },
      ],
    });
  }
  return cardFormat;
};

export default instrument;
