import React from "react";
import PanelString from "../../../value/PanelString";

const soldCourse = (data) => {
  const cardFormat = [];
  for (let index in data) {
    let dataIndex = data[index];
    let course = dataIndex.course;
    let customer = dataIndex.customer;
    let title = course.title ? course.title : "";
    let description = course.description ? course.description : "";
    let price = course.price ? course.price : "";
    let fullName = customer.fullName ? customer.fullName : "";
    let phoneNumber = customer.phoneNumber ? customer.phoneNumber : "";

    cardFormat.push({
      _id: dataIndex._id,
      // isActive: dataIndex.isActive,
      image: { value: course.cover },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
        },
        { right: [{ elementType: "text", value: description, title: description, style: { color: PanelString.color.GRAY, fontSize: "0.9em", fontWeight: "500" } }] },

        {
          left: [{ elementType: "price", value: price, direction: "ltr" }],
        },
        {
          right: [{ elementType: "text", value: fullName }],
          left: [{ elementType: "icon", value: phoneNumber, className: "icon-phone", direction: "ltr", style: { fontSize: "1em", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } }],
        },
      ],
    });
  }
  return cardFormat;
};

export default soldCourse;
