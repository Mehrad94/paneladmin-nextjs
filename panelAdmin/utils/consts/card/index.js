import instrument from "./instrument";
import gallery from "./gallery";
import country from "./country";
import mood from "./mood";
import hashtag from "./hashtag";
import genre from "./genre";
import artist from "./artist";

const card = {
  instrument,
  gallery,
  country,
  mood,
  hashtag,
  artist,
  genre
};
export default card;
