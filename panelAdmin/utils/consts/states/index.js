import addGenres from "./addGenres";
import addCategory from "./addCategory";
import addArtist from "./addArtist";
import addSlider from "./addSlider";
import addClub from "./addClub";
import upload from "./upload";
import addBanner from "./addBanner";
import addScenario from "./addScenario";
import addAlbum from "./addAlbum";
import addCountry from "./addCountry";
import instrument from "./instruments";
import addGallery from "./addGallery";
import addSong from "./addSong";
import addMood from "./addMood";
import addHashtag from "./addHashtag";

const states = {
  addScenario,
  addGenres,
  addCategory,
  addArtist,
  addSlider,
  addClub,
  upload,
  addBanner,
  addAlbum,
  addCountry,
  instrument,
  addGallery,
  addSong,
  addMood,
  addHashtag,
};
export default states;
