const addSlider = {
  Form: {
    parentType: {
      label: " ",
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "نوع والد"
      },
      childValue: [
        { name: "باشگاه", value: "CLUB" },
        { name: "تخفیف", value: "DISCOUNT" }
      ],
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    type: {
      label: "دسته بندی :",

      elementType: "inputDropDownSearch",
      elementConfig: {
        type: "text",
        placeholder: "دسته بندی"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },

    discount: {
      label: "تخفیف :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "تخفیف"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    club: {
      label: "باشگاه :",
      elementType: "inputSearch",
      elementConfig: {
        type: "text",
        placeholder: "باشگاه"
      },
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    },
    image: {
      label: "عکس :",
      elementType: "inputFile",
      kindOf: "image",
      value: "",
      validation: {
        required: true
      },
      valid: false,
      touched: false
    }
  },

  formIsValid: false
};

export default addSlider;
