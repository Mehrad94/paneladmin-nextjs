import panelAdmin from "..";
import values from "../values/index";

const menuFormat = [
  {
    title: "عمومی",
    menus: [
      {
        route: values.routes.GS_ADMIN_DASHBOARD,
        menuTitle: values.strings.DASHBOARD,
        menuIconImg: false,
        menuIconClass: "fas fa-tachometer-slowest",
        subMenu: [
          // { title: "SubDashboard", route: "/dashboard1" },
          // { title: "SubDashboard", route: "/dashboard2" }
        ],
      },
    ],
  },
  {
    title: "کاربردی",
    menus: [
      {
        route: false,
        menuTitle: values.strings.VARIABLE,
        menuIconImg: false,
        menuIconClass: "fas fa-user-music",
        subMenu: [
          {
            title: values.strings.SEE_VARIABLE,
            route: values.routes.GS_ADMIN_VARIABLE,
          },
          {
            title: values.strings.ADD_VARIABLE,
            route: values.routes.GS_ADMIN_ADD_VARIABLE,
          },
        ],
      },
    ],
  },
];

export default menuFormat;
