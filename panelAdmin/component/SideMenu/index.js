import React, { useMemo } from "react";
import Link from "next/link";
import ReactDOM from "react-dom";

// import logo from "../../assets/img/Rimtal.png";
import MainMenu from "./MainMenu";
// import "./index.scss";
// import "react-perfect-scrollbar/dist/css/styles.css";
import Scrollbar from "react-scrollbars-custom";
// import ScrollArea from "react-scrollbar";
import PerfectScrollbar from "react-perfect-scrollbar";
// const ScrollArea = require("react-scrollbar");
import panelAdmin from "../..";
const SideMenu = ({ windowLocation }) => {
  // return useMemo(() => {
  return (
    <div className={`panelAdmin-sideBar-container `}>
      <Link href={"/panelAdmin"} as={"/panelAdmin"}>
        <a className={"lgHolder"}>
          {/* <img src={logo} alt={"logo"} /> */}
          {"لوگو"}
        </a>
      </Link>

      <PerfectScrollbar>
        {/* <Scrollbar style={{ width: "100%", height: " 100% " }}> */}
        <MainMenu mainMenus={panelAdmin.menuFormat} windowLocation={windowLocation} />
      </PerfectScrollbar>
      {/* </Scrollbar> */}
    </div>
  );
  // }, []);
};

export default SideMenu;
