import React from "react";
const InputFile = (props) => {
  const {
    onChange,
    inputLabel,
    name,
    value,
    progress,
    disabled,
    accepted,
    cancelUpload,
    placeholder,
  } = props;
  console.log({ disabled, accepted });

  const elements = (
    <React.Fragment>
      <div>{value ? value : "  انتخاب نمایید ..."}</div>
      <label>
        <span onClick={disabled ? accepted : null}>
          {progress ? progress + "%" : inputLabel}
        </span>
        <input
          disabled={progress ? true : disabled}
          type="file"
          onChange={onChange}
          name={name}
        />
      </label>
    </React.Fragment>
  );
  return <div className="addFileModalContainer">{elements}</div>;
};

export default InputFile;
