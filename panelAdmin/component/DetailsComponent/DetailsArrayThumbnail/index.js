import React, { useState, Fragment } from "react";
import "./index.scss";
import ModalBox from "../../../util/modals/ModalBox";
import InputsType from "../../UI/Inputs/InputsType";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";

const DetailsArrayThumbnail = ({ cover, fieldName, sendNewVal, elementType, label, imageType, toastify }) => {
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    index: false,
    files: [...cover],
    change: false,
    kindOf: false,
  });
  const [state, setState] = useState({ remove: { index: false, name: false } });
  const stateData = {
    Form: {
      file: {
        label: label,
        elementType: elementType,
        kindOf: "image",
        value: [],
        validation: {
          required: true,
        },
        valid: false,
        touched: false,
      },
    },
    formIsValid: false,
  };
  const returnUpload = async (val) => {
    let newModalInpts = { ...ModalInpts.files };
    let InfoChange = ModalInpts.files;
    if (ModalInpts.index >= 0) {
      newModalInpts[ModalInpts.index] = val;
      InfoChange[ModalInpts.index] = newModalInpts[ModalInpts.index];
    }
    if (!ModalInpts.index) InfoChange.push(val);
    setModalInpts({ ...ModalInpts, files: InfoChange });
  };
  const _handelSendChanged = () => {
    const data = { fieldChange: fieldName, newValue: ModalInpts.files };
    sendNewVal(data);
    _handelEdit();
  };
  // =========================== modal
  // ============================= remove
  const __returnPrevstep = async (value) => {
    onHideModal();
    let newState;
    if (value) newState = ModalInpts.files.splice(state.remove.index);
    console.log({ newState });

    setState({ ...state, remove: { index: "", name: "" } });
  };
  const removeHandel = (index, name) => {
    onShowlModal({ kindOf: true });
    setState({ ...state, remove: { index, name } });
  };
  // =========================== End remove  ====================
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false });
  };
  const onShowlModal = (event) => {
    console.log({ event });

    if (event) setModalInpts({ ...ModalInpts, show: true, kindOf: event.kindOf ? event.kindOf : false, index: event.index ? event.index : false });
    else setModalInpts({ ...ModalInpts, show: true, kindOf: false, index: false });
  };
  // const renderModalInputs = <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}></ModalBox>;
  const _handelEdit = (index) => {
    setModalInpts((prev) => ({
      ...prev,
      change: !ModalInpts.change,
      files: [...cover],
    }));
  };
  const optionClick = async (event) => {
    console.log(event);

    switch (event.mission) {
      case "removeImage":
        removeHandel(event.index);
        break;
      case "edit":
        _handelEdit();
        break;
      case "editImage":
        onShowlModal({ index: String(event.index) });
        break;
      case "add":
        onShowlModal();
        break;
      case "accept":
        _handelSendChanged();
        break;
      case "cancel":
        _handelEdit();
        break;

      default:
        break;
    }
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      {ModalInpts.kindOf ? (
        <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
      ) : (
        <InputsType stateData={stateData} imageType={imageType ? imageType : fieldName} onHideModal={onHideModal} inputsData={returnUpload} showModal={ModalInpts.show} acceptedTitle="ثبت" />
      )}
    </ModalBox>
  );
  return (
    <Fragment>
      {renderModalInputs}
      <div className="card-details-row cover-image width100">
        <div className="about-title">
          <span>{"عکس های اسلاید :"}</span>
          {ModalInpts.change ? (
            <div className="btns-container">
              <a className="btns btns-add" onClick={() => optionClick({ mission: "add" })}>
                {" "}
                افزودن{" "}
              </a>
              <a className="btns btns-success" onClick={() => optionClick({ mission: "accept" })}>
                {" "}
                ثبت{" "}
              </a>
              <a className="btns btns-warning" onClick={() => optionClick({ mission: "cancel" })}>
                {" "}
                لغو{" "}
              </a>
            </div>
          ) : (
            <i onClick={() => optionClick({ mission: "edit" })} className=" icon-pencil transition0-2 rotate-84 positionUnset"></i>
          )}
        </div>
        <div className=" images-wrapper">
          {ModalInpts.change
            ? ModalInpts.files &&
              ModalInpts.files.map((slides, index) => {
                return (
                  <div key={index}>
                    <img src={slides} />
                    <i onClick={() => optionClick({ mission: "editImage", index })} className=" icon-pencil transition0-2 rotate-84"></i>
                    <i onClick={() => optionClick({ mission: "removeImage", index })} className="icon-trash-empty transition0-2 " style={{ right: "0", left: "unset" }}></i>
                  </div>
                );
              })
            : cover.map((slides, index) => {
                return (
                  <div key={index}>
                    <img src={slides} />
                  </div>
                );
              })}
        </div>
      </div>
    </Fragment>
  );
};

export default DetailsArrayThumbnail;
