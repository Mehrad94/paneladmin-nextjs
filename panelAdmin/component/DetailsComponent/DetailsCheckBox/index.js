import React, { useState } from "react";
import "./index.scss";
const DetailsCheckBox = ({ Info, Id, refreshComponent, fieldName, questions, label, sendNewVal, toastify, fixed }) => {
  const [state, setState] = useState({
    change: false,
    Info: Info,
  });

  const _handelEdit = () => {
    setState((prev) => ({
      ...prev,
      change: !state.change,
      Info: Info,
    }));
  };
  const _handelSendChanged = async () => {
    const data = { fieldChange: fieldName, newValue: state.Info.toString() };
    sendNewVal(data);

    _handelEdit();
  };
  const _handelonClick = (param) => {
    if (param) {
      setState({ ...state, Info: true });
    } else {
      setState({ ...state, Info: false });
    }
  };

  return (
    <React.Fragment>
      <div className="card-details-row">
        <div className="about-title">
          <span>{label} :</span> {state.change ? "" : !fixed && <i onClick={_handelEdit} className=" icon-pencil transition0-2 rotate-84"></i>}
          {state.change ? (
            <div className="btns-container">
              <a className="btns btns-success" onClick={_handelSendChanged}>
                {" "}
                ثبت{" "}
              </a>
              <a className="btns btns-warning" onClick={_handelEdit}>
                {" "}
                لغو{" "}
              </a>
            </div>
          ) : (
            ""
          )}
        </div>
        {state.change ? (
          <div className="checked-row" style={{ display: "flex" }}>
            <div className="centerAll">
              <label>{questions.question1} </label>
              <input type="checkbox" onChange={() => _handelonClick(true)} checked={state.Info ? true : false} value={state.Info ? "تایید شده" : "بسته شده"} />
            </div>
            <div className="centerAll">
              <label>{questions.question2} </label>
              <input type="checkbox" onChange={() => _handelonClick(false)} checked={!state.Info ? true : false} value={state.Info ? "تایید شده" : "بسته شده"} />
            </div>
          </div>
        ) : (
          <p>{Info ? questions.question1 : questions.question2}</p>
        )}
      </div>
    </React.Fragment>
  );
};

export default DetailsCheckBox;
