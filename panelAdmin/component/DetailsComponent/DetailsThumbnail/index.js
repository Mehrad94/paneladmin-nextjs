import React, { useState } from "react";
import "./index.scss";
import ModalBox from "../../../util/modals/ModalBox";
import InputsType from "../../UI/Inputs/InputsType";
const DetailsThumbnail = ({ cover, fieldName, sendNewVal, elementType, label, toastify }) => {
  const [ModalInpts, setModalInpts] = useState({
    show: false,
    files: false
  });

  const stateData = {
    Form: {
      file: {
        label: label,
        elementType: elementType,
        kindOf: "image",
        value: "",
        validation: {
          required: true
        },
        valid: false,
        touched: false
      }
    },
    formIsValid: false
  };
  const onHideModal = () => {
    setModalInpts({ ...ModalInpts, show: false, files: false });
  };
  const onShowlModal = files => {
    if (files === "files") setModalInpts({ ...ModalInpts, show: true, files: true });
    else setModalInpts({ ...ModalInpts, show: true, files: false });
  };
  const _handelSendChanged = async val => {
    const data = { fieldChange: fieldName, newValue: val };
    sendNewVal(data);
  };
  const renderModalInputs = (
    <ModalBox onHideModal={onHideModal} showModal={ModalInpts.show}>
      <InputsType stateData={stateData} imageType={fieldName} onHideModal={onHideModal} inputsData={_handelSendChanged} showModal={ModalInpts.show} acceptedTitle="ثبت" />
    </ModalBox>
  );
  return (
    <React.Fragment>
      {renderModalInputs}
      <div className="card-details-row cover-image">
        <img src={cover} />
        <i onClick={onShowlModal} className=" icon-pencil transition0-2 rotate-84"></i>
      </div>
    </React.Fragment>
  );
};

export default DetailsThumbnail;
