import { homeData } from "./homeData";
import { galleryData } from "./galleryApi";
import { artistData, artistSearchData } from "./artists";
import { instrumentData, instrumentSearchData } from "./instrumental";
import { countryData, countrySearchData } from "./country";
import { moodData, moodSearchData } from "./mood";
import { hashtagData, hashtagSearchData } from "./hashtag";
import { genreData, genreSearchData } from "./genre";
const GET = {
  homeData,
  // ================================= GALLERY
  galleryData,
  // ================================= ARTIST
  artistData,
  artistSearchData,
  // ================================= INSTRUMENT
  instrumentData,
  instrumentSearchData,
  // ================================= COUNTRY
  countryData,
  countrySearchData,
  // ================================= MOOD
  moodData,
  moodSearchData,
  // ================================= HASHTAG
  hashtagData,
  hashtagSearchData,
  // ================================= GENRE
  genreData,
  genreSearchData,
};

export default GET;
