import { takeEvery } from "redux-saga/effects";
import atSaga from "../actionTypes/saga";
import webServices from "./webServices";

// ======================================================================================= UPLOAD
export function* watchUploadImage() {
  yield takeEvery(atSaga.UPLOAD_IMAGE_DATA, webServices.POST.uploadImage);
}
// ======================================================================================= GALLERY
export function* watchGallery() {
  yield takeEvery(atSaga.GET_GALLERY_DATA, webServices.GET.galleryData);
}
// ======================================================================================= ARTIST
// export function* watchArtist() {
//   yield all([
//     takeEvery(atSaga.GET_ARTIST_DATA, webServices.GET.artistData),
//     takeEvery(atSaga.GET_SEARCH_ARTIST_DATA, webServices.GET.artistSearchData),
//   ]);
// }
export function* watchGetArtistData() {
  yield takeEvery(atSaga.GET_ARTIST_DATA, webServices.GET.artistData);
}
export function* watchSearchArtistData() {
  yield takeEvery(atSaga.GET_SEARCH_ARTIST_DATA, webServices.GET.artistSearchData);
}
// ======================================================================================= INSTRUMENTAL
export function* watchGetInstrumentalData() {
  yield takeEvery(atSaga.GET_INSTRUMENT_DATA, webServices.GET.instrumentData);
}
export function* watchSearchInstrumentalData() {
  yield takeEvery(atSaga.GET_SEARCH_INSTRUMENT_DATA, webServices.GET.instrumentSearchData);
}
// ======================================================================================= COUNTRY
export function* watchGetCountryData() {
  yield takeEvery(atSaga.GET_COUNTRY_DATA, webServices.GET.countryData);
}
export function* watchSearchCountryData() {
  yield takeEvery(atSaga.GET_SEARCH_COUNTRY_DATA, webServices.GET.countrySearchData);
}
// ======================================================================================= MOOD
export function* watchGetMoodData() {
  yield takeEvery(atSaga.GET_MOOD_DATA, webServices.GET.moodData);
}
export function* watchSearchMoodData() {
  yield takeEvery(atSaga.GET_SEARCH_MOOD_DATA, webServices.GET.moodSearchData);
}
// ======================================================================================= HASHTAG
export function* watchGetHashtagData() {
  yield takeEvery(atSaga.GET_HASHTAG_DATA, webServices.GET.hashtagData);
}
export function* watchSearchHashtagData() {
  yield takeEvery(atSaga.GET_SEARCH_HASHTAG_DATA, webServices.GET.hashtagSearchData);
}
// ======================================================================================= GENRE
export function* watchGetGenreData() {
  yield takeEvery(atSaga.GET_GENRE_DATA, webServices.GET.genreData);
}
export function* watchSearchGenreData() {
  yield takeEvery(atSaga.GET_SEARCH_GENRE_DATA, webServices.GET.genreSearchData);
}
