import panelAdmin from "../..";

export const countryInitialState = {
  countryData: null,
  searchCountryData: null,
  loading: false,
  searchLoading: false,
};

function countryReducer(state = countryInitialState, action) {
  const atRedux = panelAdmin.actionTypes.atRedux;

  switch (action.type) {
    case atRedux.SET_COUNTRY_DATA:
      return { ...state, ...{ countryData: action.data, loading: false, searchLoading: false } };
    case atRedux.SET_SEARCH_COUNTRY_DATA:
      return { ...state, ...{ searchCountryData: action.data, searchLoading: false } };
    case atRedux.START_COUNTRY_DATA:
      return { ...state, ...{ loading: true } };
    case atRedux.START_SEARCH_COUNTRY_DATA:
      return { ...state, ...{ searchLoading: true } };
    default:
      return state;
  }
}

export default countryReducer;
