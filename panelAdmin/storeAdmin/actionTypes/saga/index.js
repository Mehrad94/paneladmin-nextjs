const atSaga = {
  POST_UPLOAD_IMAGE: "POST_UPLOAD_IMAGE_SAGA",
  GET_GALLERY_DATA: "GET_GALLERY_DATA_SAGA",
  //============================================================= ARTIST
  GET_ARTIST_DATA: "GET_ARTIST_DATA_SAGA",
  GET_SEARCH_ARTIST_DATA: "GET_SEARCH_ARTIST_DATA_SAGA",
  //============================================================= INSTRUMENT
  GET_INSTRUMENT_DATA: "GET_INSTRUMENT_DATA_SAGA",
  GET_SEARCH_INSTRUMENT_DATA: "GET_SEARCH_INSTRUMENT_DATA_SAGA",
  //============================================================= COUNTRY
  GET_COUNTRY_DATA: "GET_COUNTRY_DATA_SAGA",
  GET_SEARCH_COUNTRY_DATA: "GET_SEARCH_COUNTRY_DATA_SAGA",
  //============================================================= ALBUM
  GET_ALBUM_DATA: "GET_ALBUM_DATA_SAGA",
  GET_SEARCH_ALBUM_DATA: "GET_SEARCH_ALBUM_DATA_SAGA",
  //============================================================= MOOD
  GET_MOOD_DATA: "GET_MOOD_DATA_SAGA",
  GET_SEARCH_MOOD_DATA: "GET_SEARCH_MOOD_DATA_SAGA",
  //============================================================= HASHTAG
  GET_HASHTAG_DATA: "GET_HASHTAG_DATA_SAGA",
  GET_SEARCH_HASHTAG_DATA: "GET_SEARCH_HASHTAG_DATA_SAGA",
    //============================================================= GENRE
    GET_GENRE_DATA: "GET_GENRE_DATA_SAGA",
    GET_SEARCH_GENRE_DATA: "GET_SEARCH_GENRE_DATA_SAGA",
};

export default atSaga;
