const GS_PANEL_ADMIN_TITLE = "/panelAdmin";
const GS_ADMIN_DASHBOARD = GS_PANEL_ADMIN_TITLE + "/dashboard";
const GS_ADMIN_VARIABLE = GS_PANEL_ADMIN_TITLE + "/variable";
const GS_ADMIN_ADD_VARIABLE = GS_PANEL_ADMIN_TITLE + "/addVariable";

const routes = {
  GS_ADMIN_DASHBOARD,
  GS_PANEL_ADMIN_TITLE,
  GS_ADMIN_VARIABLE,
  GS_ADMIN_ADD_VARIABLE,
};

export default routes;
