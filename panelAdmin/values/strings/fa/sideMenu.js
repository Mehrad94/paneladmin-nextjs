const DASHBOARD = "داشبورد";
const SIDEBAR_ONE_TITLE = "عمومی";
const SIDEBAR_TWO_TITLE = "کاربردی";
const SETTING_WEB = "تنظیمات سایت";
const CATEGORIES = "دسته بندی ها";
const SEE_VARIABLE = "مشاهده متغییر";
const VARIABLE = "متغییر";
const ADD_VARIABLE = "افزودن متغییر";

const sideMenu = {
  DASHBOARD,
  SIDEBAR_ONE_TITLE,
  SIDEBAR_TWO_TITLE,
  SETTING_WEB,
  CATEGORIES,
  SEE_VARIABLE,
  VARIABLE,
  ADD_VARIABLE,
};
export default sideMenu;
