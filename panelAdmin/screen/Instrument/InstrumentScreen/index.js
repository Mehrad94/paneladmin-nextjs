import React, { useEffect, useState, Fragment, useRef } from "react";
import { useSelector } from "react-redux";
import panelAdmin from "../../..";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";

const InstrumentScreen = (props) => {
  const [state, setState] = useState({
    remove: { index: "", name: "" },
    genreTitle: "",
  });

  const store = useSelector((state) => {
    return state.instrument;
  });
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const Instrument = store.instrumentData;
  const searchInstrumentData = store.searchInstrumentData;
  const InstrumentSearch = searchInstrumentData ? (searchInstrumentData.docs.length ? true : false) : false;
  const card = panelAdmin.utils.consts.card;
  console.log({ loading, searchLoading, Instrument, searchInstrumentData, InstrumentSearch });
  const optionClick = async (event) => {
    switch (event.mission) {
      case "remove":
        // removeHandel(event._id);
        break;
      default:
        break;
    }
  };
  const showDataElement = Instrument && (
    <ShowCardInformation
      data={Instrument && card.instrument(Instrument.docs)}
      // options={{ remove: true }}
      onClick={null}
      optionClick={optionClick}
      // submitedTitle={"مشاهده جزئیات"}
    />
  );

  return (
    <React.Fragment>
      {showDataElement}
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default InstrumentScreen;
