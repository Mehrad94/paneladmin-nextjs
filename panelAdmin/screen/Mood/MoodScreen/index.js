import React, { useEffect, useState, Fragment, useRef } from "react";
import { get } from "../../../api";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import { useSelector } from "react-redux";
import panelAdmin from "../../..";

const MoodScreen = (props) => {
  const { onDataChange, filters, acceptedCardInfo } = props;

  const [state, setState] = useState({ remove: { index: "", name: "" }, genreTitle: "" });
  const inputRef = useRef(null);
  const store = useSelector((state) => {
    return state.mood;
  });
  const card = panelAdmin.utils.consts.card;
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const MoodData = store.moodData;
  const searchMood = store.searchMood;
  const MoodSearch = searchMood ? (searchMood.docs.length ? true : false) : false;

  const showDataElement = MoodData && MoodData.docs.length && (
    <ShowCardInformation data={card.mood(MoodData.docs)} onClick={null} optionClick={null} />
  );
  return (
    <React.Fragment>
      {showDataElement}
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default MoodScreen;
