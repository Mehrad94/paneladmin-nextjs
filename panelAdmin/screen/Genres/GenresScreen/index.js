import React, { useEffect, useState, Fragment, useRef } from "react";
import ShowCardInformation from "../../../component/cards/ShowCardInformation";
import SpinnerRotate from "../../../component/UI/Loadings/SpinnerRotate";
import { useSelector } from "react-redux";
import panelAdmin from "../../..";
const GenresScreen = () => {
  const store = useSelector((state) => {
    return state.hashtag;
  });
  const card = panelAdmin.utils.consts.card;
  const loading = store.loading;
  const searchLoading = store.searchLoading;
  const HashtagData = store.hashtagData;
  const searchHashtag = store.searchHashtag;
  const HashtagSearch = searchHashtag ? (searchHashtag.docs.length ? true : false) : false;
  

  const showDataElement = HashtagData && HashtagData.docs.length && <ShowCardInformation data={card.hashtag(HashtagData.docs)} onClick={null} optionClick={null} />;
  return (
    <React.Fragment>
      {showDataElement}
      {/* <CountryElement Country={CountrySearch ? CountrySearch : Country} handelPage={_handelPage} tableOnclick={_tableOnclick} handelchange={handelchange} /> */}
      {loading && (
        <div className="staticStyle bgWhite">
          <SpinnerRotate />
        </div>
      )}
    </React.Fragment>
  );
};

export default GenresScreen;
