import React, { useState } from "react";
import Pageination from "../../../../components/UI/Pagination";
import "./index.scss";
import DropdownBoot from "../../../../components/UI/Inputs/DropdownBoot";
import constants from "../../../../value/constants";
import table from "../../../../util/consts/table";
import TabelBasic from "../../../../components/Tables";
import IsNull from "../../../../components/UI/IsNull";
const SongElement = (props) => {
  const { Song, handelPage, handelchange, showModal, state, setState } = props;
  const dropDown = [
    { value: constants.ALBUM, title: "آلبوم ها" },
    { value: constants.PLAY_LIST, title: " لیست پخش" },
    { value: constants.FLAG, title: "کشور ها " },
    { value: constants.SONG, title: "آهنگ ها" },
    { value: constants.ARTIST, title: "خواننده ها" },
  ];
  let dropDownData = [];
  for (const index in dropDown) dropDownData.push({ value: dropDown[index].value, title: dropDown[index].title });

  const clickedDropDone = (value, title) => {
    setState({ ...state, valueEn: value, valueFa: title });
  };
  const tableOnclick = (index, work) => {
    showModal({ work, index });
    console.log({ index, work });
  };
  return (
    <div className="countainer-main  ">
      <div className="main-head">
        <div>
          {" "}
          <div className="subtitle-elements">
            <div onClick={() => showModal({ work: "add" })} className="btns btns-add addImageBtn ">
              {"بارگزاری آهنگ"}
            </div>
            <div className="btns-container">{/* <DropdownBoot dropDownData={dropDownData} accepted={clickedDropDone} value={state.valueFa} /> */}</div>
          </div>
        </div>
      </div>
      <div className={"centerAll"}>
        <div className="elemnts-box  boxShadow tableMainStyle ">
          {Song ? (
            <TabelBasic
              subTitle={
                <div className="disColum ">
                  <h4 style={{ color: "black", paddingBottom: "0.2em", fontSize: "1.5em" }}>{"آهنگ های سایت"}</h4>
                  {/* <span style={{ fontSize: "0.9em" }}>{`   تعداد کل : ${Song.total}  `}</span>{" "} */}
                </div>
              }
              imgStyle={{ width: "2em", height: "2em", borderRadius: "0.4em" }}
              tbody={table.song(Song.docs).tbody}
              thead={table.song(Song.docs).thead}
              onClick={tableOnclick}
              inputRef
              // searchHead={InputVsIcon({ name: "genreTitle", icon: "far fa-search", placeholder: "pop,rock,...", onChange: handelchange, dir: "ltr" })}
            />
          ) : (
            <IsNull title={"آهنگی موجود نمی باشد"} />
          )}
        </div>
      </div>

      {Song && Song.pages >= 2 && <Pageination limited={"3"} pages={Song ? Song.pages : ""} activePage={Song ? Song.page : ""} onClick={handelPage} />}
    </div>
  );
};

export default SongElement;
