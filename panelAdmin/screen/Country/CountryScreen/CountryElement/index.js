import React from "react";
import table from "../../../../util/consts/table";
import Pageination from "../../../../components/UI/Pagination";
import InputVsIcon from "../../../../components/UI/InputVsIcon";
import TabelBasic from "../../../../components/Tables";

const CountryElement = (props) => {
  const { Country, handelPage, tableOnclick, handelchange } = props;
  return (
    <div className="countainer-main centerAll ">
      <div className="elemnts-box  boxShadow tableMainStyle">
        <TabelBasic
          subTitle={
            <div className="disColum ">
              <h4 style={{ color: "black", paddingBottom: "0.2em", fontSize: "1.5em" }}>{" کشور ها"}</h4>
              <span style={{ fontSize: "0.9em" }}>{`   تعداد کل : ${Country.total}  `}</span>{" "}
            </div>
          }
          imgStyle={{ width: "2em", height: "2em", borderRadius: "100%" }}
          tbody={table.country(Country.docs).tbody}
          thead={table.country(Country.docs).thead}
          onClick={tableOnclick}
          inputRef
          searchHead={InputVsIcon({ name: "genreTitle", icon: "far fa-search", placeholder: "Iran , United Stated ,...", onChange: handelchange, dir: "ltr" })}
        />

        {Country && Country.pages >= 2 && <Pageination limited={"3"} pages={Country ? Country.pages : ""} activePage={Country ? Country.page : ""} onClick={handelPage} />}
      </div>
    </div>
  );
};

export default CountryElement;
