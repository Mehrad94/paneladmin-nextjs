import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const slider = async (id, param, setIsLoading) => {
  let URL = Strings.ApiString.SLIDER;
  setIsLoading(true);
  console.log({ apiParam: param });
  return axios
    .patch(URL + "/" + id, param)
    .then((Response) => {
      setIsLoading(false);
      console.log({ Response });
      if (Response.data);
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch((error) => {
      console.log({ error });
      setIsLoading(false);
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default slider;
