import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const category = async (id, param) => {
  console.log(param);

  return axios
    .patch(Strings.ApiString.CATEGORY + "/" + id, param)
    .then(category => {
      console.log({ category });
      toastify("با موفقیت ثبت شد", "success");
      return true;
    })
    .catch(error => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else if (error.response.data)
        switch (error.response.data.Error) {
          case 1016:
            toastify("وزن تکراری می باشد", "error");
            break;
          case 1017:
            toastify("عنوان انگلیسی تکراری می باشد", "error");
            break;
          case 1018:
            toastify("عنوان فارسی تکراری می باشد", "error");
            break;
          default:
            toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
            break;
        }
      return false;
    });
};

export default category;
