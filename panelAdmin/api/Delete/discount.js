import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const discount = async param => {
  let URL = Strings.ApiString.DISCOUNT;
  return axios
    .delete(URL + "/" + param)
    .then(Response => {
      console.log({ Response });
      // setLoading(false);
      if (Response.data);
      toastify("با موفقیت حذف شد", "success");
      return true;
    })
    .catch(error => {
      console.log({ error });

      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");

      return false;
    });
};
export default discount;
