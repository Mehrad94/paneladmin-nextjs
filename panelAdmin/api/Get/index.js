// import categoreis from "./categoreis";
// import sliders from "./sliders";
// import Owners from "./Owners";
// import discounts from "./discounts";
// import discount from "./discount";
// import categoreisSearch from "./categoreisSearch";
// import ownersSearch from "./ownersSearch";
// import discountSearch from "./discountSearch";
import genres from "./genres";
// import club from "./club";
import genreSearch from "./genreSearch";
// import category from "./category";
// import members from "./members";
// import banners from "./banners";
// import scenarios from "./scenarios";
import artistSearch from "./artistSearch";
import country from "./country";
import countrySearch from "./countrySearch";
import instrument from "./instrument";
import instrumentSearch from "./instrumentSearch";
// import gallery from "./gallery";
// import song from "./song";
import artists from "./artists";
import mood from "./mood";
import hashtag from "./hashtag";

const get = {
  instrument,
  instrumentSearch,
  // categoreis,
  // sliders,
  // Owners,
  // discounts,
  // categoreisSearch,
  // category,
  // ownersSearch,
  // discountSearch,
  genres,
  genreSearch,
  // discount,
  // club,
  // members,
  // banners,
  // scenarios,
  artistSearch,
  country,
  countrySearch,
  // gallery,
  // song,
  artists,
  mood,
  hashtag,
};
export default get;
