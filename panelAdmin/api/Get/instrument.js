import axios from "../axios-orders";
import panelAdmin from "../..";

const instrument = async ({ page }) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.INSTRUMENT;
  const axiosData = page ? strings + "/" + page : strings;
  return axios.get(axiosData);
  // .then((instrument) => {
  //   console.log({ instrument });
  //   returnData(instrument.data);
  //   loading(false);
  // })
  // .catch((error) => {
  //   console.log({ error });
  //   if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
  //   else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
  // });
};

export default instrument;
