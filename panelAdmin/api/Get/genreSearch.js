import axios from "../axios-orders";
import panelAdmin from "../..";

const genreSearch = async (param, page, returnData) => {
  // console.log({ param, page, returnData });
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString;
  return axios
    .get(strings.GENRES + "/" + param + "/" + page)
    .then((genreSearch) => {
      console.log({ genreSearch });
      returnData(genreSearch.data);
      // loading(false);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default genreSearch;
