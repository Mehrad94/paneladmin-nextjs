import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const club = async (paramId, returnData, loading) => {
  console.log({ paramId });

  return axios
    .get(Strings.ApiString.CLUB + "/" + paramId)
    .then(club => {
      console.log({ club });
      returnData(club.data);
      // loading(false);
      return true;
    })
    .catch(error => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return false;
    });
};

export default club;
