import axios from "../axios-orders";
import panelAdmin from "../..";

const instrumentSearch = async (param, page, returnData) => {
  const toastify = panelAdmin.utils.toastify;
  const strings = panelAdmin.values.apiString.INSTRUMENT;

  return axios
    .get(strings + "/" + param + "/" + page)
    .then((instrumentSearch) => {
      // console.log({ instrumentSearch });
      returnData(instrumentSearch.data);
      // loading(false);
    })
    .catch((error) => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default instrumentSearch;
